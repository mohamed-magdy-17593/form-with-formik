/** @jsx jsx */
import {jsx} from '@emotion/core'
import {Component} from 'react'
import Input from '../../shared/components/Input'
import {Formik, Form} from 'formik'
import {Button} from '../../shared/components/Button'
import InputContainer from '../../shared/components/InputContainer'
import * as yup from 'yup'
import TextInput from '../../shared/components/TextInput'
import {memoValidation} from '../../utils/memo'
import Row from '../../shared/components/Row'
import Select from '../../shared/components/Select'
import Countries from '../../api/container/Countries'
import Cities from '../../api/container/Cities'
import ActivityTypes from '../../api/container/ActivityTypes'
import {GoogleMap, Marker, withGoogleMap} from 'react-google-maps'
import MultiSelect from '../../shared/components/MultiSelect'
import {GeoPosition} from 'react-fns'

// mocking
const isCool = str =>
  new Promise(resolve => setTimeout(() => resolve(str), 1000))

const MyMap = withGoogleMap(props => (
  <GoogleMap defaultZoom={8} {...props}>
    <Marker position={props.center} />
  </GoogleMap>
))

const Map = props => (
  <MyMap
    {...props}
    loadingElement={<div style={{height: `100%`}} />}
    containerElement={<div style={{height: `400px`}} />}
    mapElement={<div style={{height: `100%`}} />}
  />
)

const geocoder = new window.google.maps.Geocoder()

class RunAction extends Component {
  static defaultProps = {
    action: () => {},
  }
  componentDidMount() {
    this.props.action()
  }
  render() {
    return null
  }
}

// prettier- ignore
function JoinForm() {
  return (
    <Formik
      initialValues={{
        title: '',
        contactName: '',
        email: '',
        notes: '',
        password: '',
        confirmPassword: '',
        country: '',
        city: '',
        activities: [],
        address: '',
        lat: 0,
        lng: 0,
      }}
      validationSchema={yup.object({
        address: yup.string().required(),
        email: yup
          .string()
          .email('الايميل غلط')
          .required(),
        contactName: yup.string().required(),
        password: yup
          .string()
          .min(8)
          .max(15)
          .required('Password is required'),
        confirmPassword: yup
          .string()
          .oneOf([yup.ref('password'), null], "Passwords don't match")
          .required('Password confirm is required'),
        country: yup.string().required(),
        city: yup.string().required(),
        activities: yup
          .array()
          .min(1)
          .required(),
      })}
      onSubmit={(values, {setSubmitting, resetForm}) => {
        setTimeout(() => {
          alert(JSON.stringify(values, null, 2))
          setSubmitting(false)
          resetForm()
        }, 1000)
      }}
    >
      {({values, isSubmitting, handleReset, setFieldError, setFieldValue}) => (
        <Form>
          <GeoPosition>
            {({coords}) => (
              <RunAction
                key={JSON.stringify(coords)}
                action={() => {
                  if (coords) {
                    const {latitude: lat, longitude: lng} = coords
                    geocoder.geocode(
                      {location: {lat, lng}},
                      (results, status) => {
                        if (status === 'OK') {
                          setFieldValue('address', results[0].formatted_address)
                        }
                      },
                    )
                    setFieldValue('lat', lat)
                    setFieldValue('lng', lng)
                  }
                }}
              />
            )}
          </GeoPosition>
          <InputContainer>
            <Input
              name="title"
              validate={memoValidation(
                'join:title',
                yup
                  .string()
                  .test('title', 'name is not cool', value => {
                    setFieldError('title', '...')
                    return isCool(value).then(v => v === 'cool')
                  })
                  .required(),
              )}
              label="Business name*"
              placeholder="Business name"
            />
          </InputContainer>
          <InputContainer>
            <ActivityTypes>
              {({activityTypes, loading}) => (
                <MultiSelect
                  key={loading}
                  name="activities"
                  label="Activities*"
                  placeholder="Select all activities"
                  options={activityTypes}
                  isLoading={loading}
                />
              )}
            </ActivityTypes>
          </InputContainer>
          <br />
          <InputContainer>
            <Input
              name="address"
              label="Address*"
              placeholder="Address"
              onChange={value => {
                geocoder.geocode({address: value}, (results, status) => {
                  if (status === 'OK') {
                    const {lat, lng} = results[0].geometry.location
                    setFieldValue('lat', lat())
                    setFieldValue('lng', lng())
                  }
                })
              }}
            />
          </InputContainer>
          <InputContainer>
            <Map
              center={{lat: values.lat, lng: values.lng}}
              onClick={({latLng: {lat, lng}}) => {
                lat = lat()
                lng = lng()
                geocoder.geocode({location: {lat, lng}}, (results, status) => {
                  if (status === 'OK') {
                    setFieldValue('address', results[0].formatted_address)
                  }
                })
                setFieldValue('lat', lat)
                setFieldValue('lng', lng)
              }}
            />
          </InputContainer>
          <br />
          <InputContainer>
            <Input
              name="contactName"
              label="Contact person name*"
              placeholder="Contact person name"
            />
          </InputContainer>
          <br />
          <InputContainer>
            <div>
              <Row columnNumber={2}>
                <div>
                  <Countries>
                    {({countries, loading}) => {
                      return (
                        <Select
                          key={loading}
                          name="country"
                          label="countres*"
                          placeholder="choose country"
                          options={countries}
                          isLoading={loading}
                        />
                      )
                    }}
                  </Countries>
                </div>
                <div>
                  <Cities variables={{CountryID: values.country || 0}}>
                    {({cities, loading}) => {
                      return (
                        <Select
                          key={loading}
                          name="city"
                          label="cities*"
                          placeholder="choose city"
                          options={cities}
                          isLoading={loading}
                        />
                      )
                    }}
                  </Cities>
                </div>
              </Row>
            </div>
          </InputContainer>
          <br />
          <InputContainer>
            <Input name="email" label="Email*" placeholder="Email" />
          </InputContainer>
          <InputContainer>
            <Input
              type="password"
              name="password"
              label="Password*"
              placeholder="Password"
            />
          </InputContainer>
          <InputContainer>
            <Input
              type="password"
              name="confirmPassword"
              label="confirm password*"
              placeholder="confirm password"
            />
          </InputContainer>
          <br />
          <InputContainer>
            <TextInput
              name="notes"
              label="Tell Us About Your Business"
              placeholder="Provide details about your business"
              rows={4}
            />
          </InputContainer>
          {/* actions */}
          <InputContainer css={{display: 'flex', flexDirection: 'row-reverse'}}>
            <Button type="submit" disabled={isSubmitting}>
              Submit
            </Button>
            <Button
              outline
              type="button"
              css={{marginRight: 12}}
              onClick={handleReset}
            >
              Reset
            </Button>
          </InputContainer>
        </Form>
      )}
    </Formik>
  )
}

// const options = [
//   {value: 'chocolate', label: 'Chocolate'},
//   {value: 'strawberry', label: 'Strawberry'},
//   {value: 'vanilla', label: 'Vanilla'},
// ]

export default JoinForm
