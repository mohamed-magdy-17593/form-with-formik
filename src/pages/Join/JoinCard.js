/** @jsx jsx */
import {jsx} from '@emotion/core'
import styled from '@emotion/styled'

import {fullWidth, alignCenter} from '../../shared/styles/helpers'
import JoinForm from './JoinForm'

const Card = styled.div(fullWidth, {
  padding: 24,
  background: 'white',
  borderRadius: 4,
})

function JoinCard() {
  return (
    <Card>
      <h3 css={[alignCenter, {marginBottom: 8}]}>Join us</h3>
      <JoinForm />
    </Card>
  )
}

export default JoinCard
