import * as R from 'ramda'

export function lastIndex(x) {
  return x.length - 1
}

export function last(x) {
  return x[lastIndex(x)]
}

export const log = R.curry((tag, a) => {
  console.log(tag, a)
  return a
})
