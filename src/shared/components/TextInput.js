/** @jsx jsx */
import {jsx} from '@emotion/core'
import {InputStyle} from './Input'
import EssentialFieldWrappers from './EssentialFieldWrappers'

const TextInputStyle = InputStyle.withComponent('textarea')

function TextInput({name, validate, label, ...restProps}) {
  return (
    <EssentialFieldWrappers name={name} validate={validate} label={label}>
      {({field}) => <TextInputStyle {...field} {...restProps} />}
    </EssentialFieldWrappers>
  )
}

export default TextInput
