/** @jsx jsx */
import {jsx} from '@emotion/core'
import styled from '@emotion/styled'
import {gray, dark} from '../styles/variables'
import {fullWidth} from '../styles/helpers'
import EssentialFieldWrappers from './EssentialFieldWrappers'

const InputStyle = styled.input(
  fullWidth,
  {
    outline: 'none',
    border: `1px solid ${gray}`,
    background: 'white',
    color: dark,
    padding: 12,
    borderRadius: 4,
    '::placeholder': {
      color: '#ccc',
    },
  },
  ({error}) => (error ? {borderColor: 'red'} : {':focus': {borderColor: dark}}),
)

function Input({name, validate, label, onChange = () => {}, ...restProps}) {
  return (
    <EssentialFieldWrappers name={name} validate={validate} label={label}>
      {({field, form: {setFieldValue}}) => (
        <InputStyle
          {...field}
          onChange={e => {
            setFieldValue(field.name, e.target.value)
            onChange(e.target.value)
          }}
          {...restProps}
        />
      )}
    </EssentialFieldWrappers>
  )
}

export {InputStyle}
export default Input
